
(function(){
			angular.module('todoApp',
				[
				'ui.router',
				'ngMaterial'
				])
  			.controller('TodoListController',TodoListController)
  			.config(function($stateProvider, $urlRouterProvider) {
  			$urlRouterProvider.otherwise("/state1");
			  $stateProvider
			    .state('state1', {
			      url: "/state1",
			      templateUrl: "app/temp/state1.html",
			      controller:'TodoListController'
			    })
			   	.state('state2', {
			      url: "/state2",
			      templateUrl: "app/temp/state2.html",
			      controller:'TodoListController'
			    })
			    .state('state3', {
			      url: "/state3",
			      templateUrl: "app/temp/state3.html",
			      controller:'TodoListController'
			    })
			    ;
			})
			.config(function($mdThemingProvider) {
			  $mdThemingProvider.theme('default')
			   	 .primaryPalette('pink')
			    .accentPalette('pink')
			    .warnPalette('blue');
			});
  			TodoListController.$inject = ['$state','$scope','$interval'];
  			function TodoListController($state,$scope,$interval) {
  				var lnk=this;
  				lnk.login=login;
  				lnk.next=next;
  				lnk.home=home;
  				lnk.activated = false;
			    $scope.data = {
			      group1 : 'Excellent',
			      group2 : '2',
			      group3 : 'avatar-1'
			    };
			    $scope.avatarData = [{
			        id: "avatars:svg-1",
			        title: 'avatar 1',
			        value: 'avatar-1'
			      },{
			        id: "avatars:svg-2",
			        title: 'avatar 2',
			        value: 'avatar-2'
			      },{
			        id: "avatars:svg-3",
			        title: 'avatar 3',
			        value: 'avatar-3'
			    }];
			    $scope.radioData = [
			      { label: '1', value: 1 },
			      { label: '2', value: 2 },
			      { label: '3', value: '3', isDisabled: true },
			      { label: '4', value: '4' }
			    ];
			    $scope.submit = function() {
			      alert('submit');
			    };
			    $scope.addItem = function() {
			      var r = Math.ceil(Math.random() * 1000);
			      $scope.radioData.push({ label: r, value: r });
			    };
			    $scope.removeItem = function() {
			      $scope.radioData.pop();
			    };
  				function login()
  				{
  					$state.go('state2');
  				}
  				function next()
  				{
  					$state.go('state3');
  				}
  				function home()
  				{
  					$state.go('state1');
  				}
  				}

})();